$(document).ready(function(){

		$('.btn_input').on('click', function(e){
			e.preventDefault();

			var input_value = $('.input_value').val(),
					input_date  = $('.input_date').val();



			if(input_date === '' || input_value === '' ){
				sweetAlert("Oops...", "Favor preencher todos os campos!", "error");
				return false;
			}
			
			var newInput = {
				value: input_value,
				date: input_date,
				tipo: 'input'
			};

				session.save('finance', newInput);
		});



		$('.btn_output').on('click', function(e){
			e.preventDefault();

			var output_value = $('.output_value').val(),
					output_date  = $('.output_date').val();

			if(output_value == '' || output_date == '' ){
				sweetAlert("Oops...", "Favor preencher todos os campos!", "error");
				return false;
			}

			var newOutput = {
				value: output_value,
				date:  output_date,
				tipo: 'output'
			};

			session.save('finance', newOutput);
					
		});


		$('.btn_clear').on('click', function(e){
			e.preventDefault();

			session.destroy();

		});

		$(".date").mask("99/99/9999");

});		