var session = {
	save: function(nameItem, newItem){
			// Se o item desejado existir na sessao, entao a variavel
			// currentSession e igual ao item, se nao a variavel e igual um array vazio
			var currentSession = JSON.parse(localStorage.getItem(nameItem)) || [];
			currentSession.push(newItem);
			localStorage.setItem(nameItem, JSON.stringify(currentSession));
			swal("Salvo com sucesso", "Dados salvos com sucesso.", "success"); 

	},
	get: function(){
		return localStorage.getItem('finance');
	},

	destroy: function(){
		swal({   
			title: "Tem certeza disso?",
			text: "Dados não podem ser recuperados.",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Sim",   
			closeOnConfirm: false }, 
			function(){  
				localStorage.clear(); 
				swal("Dados apagados!", "Seus dados foram apagados com sucesso.", "success"); 
			});
	},
	verify: function(){
		if(localStorage.getItem('finance')  === null) {
			sweetAlert("Oops...", "Ainda não há dados cadastrados!", "error");
		}
	}
}