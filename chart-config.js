var lineChartDataInput = {
			labels : reports.getDatesInput(),
			datasets : [
				{
					label: "My First dataset",
					fillColor : "rgba(0, 204, 0, 0.2)",
					strokeColor : "rgba(0, 204, 0, 1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : reports.getValuesInput()
				}
			]

		};

		var lineChartDataOutput = {
			labels : reports.getDatesOutput(),
			datasets : [
				{
					label: "My First dataset",
					fillColor : "rgba(255, 0, 0, 0.2)",
					strokeColor : "rgba(255, 0, 0, 1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : reports.getValuesOutput()
				}
			]

		};

		var lineChartDataInputOutput = {
			labels : reports.getDates(),
			datasets : [
				{
					label: "My First dataset",
					fillColor : "rgba(0, 153, 255, 0.2)",
					strokeColor : "rgba(0, 153, 255,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : reports.getValues()
				}
			]

		};

		$(document).ready(function(){

			// Get context with jQuery - using jQuery's .get() method.
			var ctx = $("#input").get(0).getContext("2d");
			// This will get the first returned node in the jQuery collection.
			var myNewChart = new Chart(ctx).Line(lineChartDataInput, {
						responsive: true
					});


						// Get context with jQuery - using jQuery's .get() method.
			ctx = $("#output").get(0).getContext("2d");
			// This will get the first returned node in the jQuery collection.
			var myNewChart = new Chart(ctx).Line(lineChartDataOutput, {
						responsive: true
					});

									// Get context with jQuery - using jQuery's .get() method.
			ctx = $("#inputoutput").get(0).getContext("2d");
			// This will get the first returned node in the jQuery collection.
			var myNewChart = new Chart(ctx).Line(lineChartDataInputOutput, {
						responsive: true
					});

			});
