var reports = {	
	getDates: function(){
		var finances = JSON.parse(session.get());
		var dates = [];
		var uniqueDates = [];
		var lastDate = '00/00/0000';

		//monto o array com todas as datas
		for(i in finances) {
			dates.push(finances[i].date);
		}

		//ordeno as datas
		dates.sort();

		for(i in dates) {
			// Se a ultima data for diferente da data atual
			// Faço isso pois existe varios lançamentos com datas iguais, mas
			// o array so podera ser montado com datas diferentes
			if(lastDate != dates[i]) {
				uniqueDates.push(dates[i]);
			}

			lastDate = dates[i];
		}

		return uniqueDates;		
	},

	getValues: function(){
		var finances = JSON.parse(session.get());
		var values = [];
		var inputsValues = 0;
		var outputsValues = 0;
		var lastDate = '00/00/0000';
		var lastTipo;
		var total = 0;

		for(i in finances){

			// Entra no if se a ultima data for diferente da data atual OU se a ultima data for igual a data atual 
			// E se o ultimno tipo for diferente do tipo atual
			if(lastDate != finances[i].date || (lastDate === finances[i].date && lastTipo != finances[i].tipo)) {
				for(j in finances){
					if(finances[i].date === finances[j].date  && finances[j].tipo === 'input'){
						inputsValues = inputsValues + parseInt(finances[j].value);
					}
				
					if(finances[i].date === finances[j].date && finances[j].tipo === 'output'){	
						outputsValues = outputsValues + parseInt(finances[j].value);
					}
				}
			}

			// O valor so pode ser mostrado uma vez por data 
			if(lastDate != finances[i].date)
			{
				total = total + (inputsValues - outputsValues);
				values.push(total);
			}

			lastDate = finances[i].date;
			lastTipo = finances[i].tipo;
			inputsValues = 0;
			outputsValues = 0;
		}

		return values;
	},

	getDatesInput: function(){
		var finances = JSON.parse(session.get());
		var dates = [];

		for(i in finances) {
			if(finances[i].tipo == 'input')
				dates.push(finances[i].date);
		}

		return dates;	
	},

	getValuesInput: function(){
		var finances = JSON.parse(session.get());
		var values = [];

		for(i in finances){
			if(finances[i].tipo == 'input')
				values.push(finances[i].value)
		}

		return values;
	},

	getDatesOutput: function(){
		var finances = JSON.parse(session.get());
		var dates = [];

		for(i in finances) {
			if(finances[i].tipo == 'output')
				dates.push(finances[i].date);
		}

		return dates;	
	},

	getValuesOutput: function(){
		var finances = JSON.parse(session.get());
		var values = [];

		for(i in finances){
			if(finances[i].tipo == 'output')
				values.push(finances[i].value)
		}

		return values;
	},

};

